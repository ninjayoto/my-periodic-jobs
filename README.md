![build status](https://gitlab.com/ninjayoto/my-periodic-jobs/badges/master/build.svg)
# Sample gitlab-ci project
The tutorial can be found here:
[56 seconds to get gitlab to do periodic jobs for you!](https://medium.com/@YYC_Ninja/56-seconds-to-get-gitlab-to-do-periodic-jobs-for-you-6a731b977559)

Basically gitlab-ci runs 2 commands daily:
- [ ] GET bitcoin price in Candian dollars
- [ ] POST  it to [putsreq.com](https://putsreq.com/wkDdMQWhaOyalisaIe49/inspect)
 
